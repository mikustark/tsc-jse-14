package ru.tsc.karbainova.tm;

import ru.tsc.karbainova.tm.component.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}

